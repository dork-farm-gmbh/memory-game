// import ConfettiGenerator from "confetti-js";
import wsClient from '../../services/ws.js'
const Game = {
    props: [],
    data() {
        return {
            devMode: true, //show only 2 pairs of unshuffled cards
            // ws: new WebSocket(wsUrl),
            imagesType: 'cartoons',
            images: {
                cartoons: ["aladin", "ariel", "baghera", "donald", "dumbo", "elsa", "minion", "simba"],
            },            
            gameState: {
                cards: [],
                players: [], //[{ name: '', points: 0, nextInTurn: false, winner: false, online: false }]
            },
            playerIndex: null, // index in the array of gameState.players
            playerName: null,
            publicPath: process.env.BASE_URL,
            errMsg: ''
        }
    },
    mounted() {
        this.gameState.cards = this.getCards()
        // create new web socket client and send it a function to call when data is received
        this.wsClient = new wsClient(this.onServerMessage)
    },
    methods: {
        getCards() {
            const images = this.devMode
                ? [...this.images[this.imagesType].slice(0, 2), ...this.images[this.imagesType].slice(0, 2)] // duplicate first 2 images from the image set
                : [...this.images[this.imagesType], ...this.images[this.imagesType]]
                    .sort(() => Math.random() - 0.5) //duplicate the original image set and shuffle it
            return images.map(img => ({ img, isFlipped: false, isFound: false }))
        },
        onServerMessage(data) {
            console.debug('msg', data)
            switch (data.type) {
                case 'GAME_STATE':
                    this.gameState = data.payload
                    if(this.playerIndex === null) {
                        this.gameState.players.forEach((player, index) => {
                            if(player.name === this.playerName) {
                                this.playerIndex = index
                            }
                        });
                    }
                    break;
                case 'NAME_ERR':
                    this.errMsg = data.payload
                    this.playerName = null
                    this.playerIndex = null
                    break;
                default:
                    break;
            }
        },
        savePlayerName() {
            console.log('send new playwer');
            this.wsClient.send({type: 'NEW_PLAYER', payload: this.playerName})
            this.wsClient.send({type: 'NEW_CARDS', payload: this.gameState.cards})
        },
        flip(index) {
            console.log('flip called');
            // if it's oponent's turn, do nothing
            if (this.gameState.nextInTurn !== this.gameState.player.name) {
                return
            }

            // for 0 or 1 cards already flipped
            if (this.flippedCards.length <= 1) {
                // flip the card
                this.gameState.cards[index].isFlipped = true;
            }

            // if another card was already flipped
            if (this.flippedCards.length === 2) {
                // if they match, mark them as found
                if (this.flippedCards[0].img === this.gameState.cards[index].img) {
                    this.flippedCards[0].isFound = true
                    this.gameState.cards[index].isFound = true
                    this.gameState.player.points++

                    // check if game is done => if no card  left with isFound === true
                    if (!this.gameState.cards.find(card => !card.isFound)) {
                        this.gameState.winner = (this.gameState.player.points > this.gameState.oponent.points)
                            ? this.gameState.winner = this.gameState.player.name
                            : this.gameState.oponent.name
                    }
                } else {
                    this.gameState.nextInTurn = this.gameState.oponent.name
                    setTimeout(() => {
                        this.flippedCards[0].isFlipped = false
                        this.gameState.cards[index].isFlipped = false
                        this.sendDataToOponent()
                    }, 2000);
                }
            }
            this.sendDataToOponent()
        }
    },
    computed: {
        flippedCards() {
            return this.gameState.cards.filter(card => card.isFlipped && !card.isFound)
        }
    }
}

export default Game