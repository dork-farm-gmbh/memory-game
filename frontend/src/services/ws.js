export default class wsClient {
    /**
     * 
     * @param function onmessage - a function that is called when a server message is not 'ping'
     */
    constructor(onmessage) {
        this.wsUrl = 'ws://localhost:3030'
        this.client = null
        this.onmessage = onmessage
        this.start()
    }

    start() {
        this.client = new WebSocket(this.wsUrl)
        this.client.onopen = () => {
            console.log('connected to ws server') // on connecting, do nothing but log it to the console
        }

        this.client.onmessage = evt => {
            if (evt.data === 'ping') {
                this.client.send('pong')
                return
            }
            this.onmessage(JSON.parse(evt.data))
        }

        this.client.onclose = event => {
            if (event.wasClean) {
                console.log(`[close] ws connection closed cleanly, code=${event.code} reason=${event.reason}`)
            } else {
                // e.g. server process killed or network down; event.code is usually 1006 in this case
                console.error(`[close] ws connection died, code=${event.code} reason=${event.reason}`)
                // try to reconnect
                this.start()
            }
        }
        this.client.onerror = (event) => {
            console.error("WebSocket error observed:", event)
        }
    }

    send(msg) {
        this.client.send(JSON.stringify(msg))
    }
}