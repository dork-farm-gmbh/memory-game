const WebSocket = require('ws');
const wss = new WebSocket.Server({ port: 3030 });

let gameState = {players: [], cards: []}

addNewPlayer = (name) => {
    gameState.players.push({
        name: name,
        points: 0,
        nextInTurn: false,
        winner: false,
        online: true 
    })
    if(gameState.players.length === 1) {
        gameState.players[0].nextInTurn = true
    }
}

processData = (ws, data) => {
    switch (data.type) {
        case 'NEW_PLAYER':
            let hasErr = false
            gameState.players.forEach((player, index) => {
                if (player.name.toLowerCase() === data.payload.toLowerCase()) {
                    console.log('err', {type: 'NAME_ERR', payload: 'Cineva cu același nume e deja în joc!'});
                    ws.send(JSON.stringify({type: 'NAME_ERR', payload: 'Cineva cu același nume e deja în joc!'}))
                    hasErr = true
                }
            })
            if(!hasErr) {
                addNewPlayer(data.payload)
            }
            break;
        case 'NEW_CARDS':
            if(gameState.cards.length === 0) {
                gameState.cards = data.payload
            }
            break
        default:
            break;
    }
    return
}

wss.on('connection', function connection(ws) {
    ws.isAlive = true;

    ws.on('message', function incoming(data) {
        this.isAlive = true;

        // console.log('wss.clients', wss.clients);
        if (data !== 'pong') {
            console.log('received data:', data);
            console.log('gameState:', gameState);
            data = JSON.parse(data)
            
            //read the received message and process it
            processData(ws, data)

            // send update gameState to all clients/players
            console.log('updated gameState:', gameState);
            console.log('------------');
            console.log('------------');
            wss.clients.forEach(function each(client) {
                if (client.readyState === WebSocket.OPEN) {
                    // console.log('data', JSON.stringify(data));
                    client.send(JSON.stringify({type: 'GAME_STATE', payload: gameState}));
                }
            });
        }
    });
});

const interval = setInterval(function ping() {
    wss.clients.forEach(function each(client) {
        if (client.isAlive === false) return client.terminate();

        client.isAlive = false;
        client.send('ping');
    });
}, 2000);

wss.on('close', function close() {
    clearInterval(interval);
});